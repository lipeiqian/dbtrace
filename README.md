### dbtrace
----------
dbtrace 是基于Java语言开发的数据库访问性能诊断工具，设计目标是使应用程序零代码修改、无缝集成到应用中，轻松完成Java应用中JDBC访问的性能诊断、耗时跟踪、调用栈跟踪及日志记录。

----------
#### dbtrace的特点
- 对数据库dba分析sql性能、应用开发者修改程序逻辑友好
- 日志输出显示直观（控制台、日志等）
- 集成配置简单

----------
**使用文档**

#### 安装
maven项目在pom.xml文件中添加依赖
```xml
<dependency>
	<groupId>john.walker</groupId>
	<artifactId>dbtrace</artifactId>
	<version>1.1.0-SNAPSHOT</version>
</dependency>
```
#### 配置
在src/main/resources目录下增加配置文件dbtrace.xml，其一般配置内容如下：
```xml
<?xml version="1.0" encoding="UTF-8"?>
<root xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="dbtrace.xsd"></root>
<monitor>
	<!-- 是否开启sql监控 -->
	<queryLog>true</queryLog>
	
	<!-- 只打印指定耗时及以上的sql语句，单位毫秒 -->
	<queryLogThreshold>0</queryLogThreshold>
	
	<!-- 开启sql监控，是否打印耗调用栈 -->
	<queryLogTrace>true</queryLogTrace>
	
	<!-- 显示sql打印的日子文件路径，（必须添加文件Logger） -->
	<queryLogFile></queryLogFile>

	<!-- 日志组件配置 -->
	<loggers>
		<!-- 添加 日志打印组件 ，也可以继承AbstractLogger类，实现相应方法  -->
		<logger>john.walker.log.ConsoleLogger</logger>
		<logger>john.walker.log.FileLogger</logger>
		<!-- <logger>john.walker.log.Log4jLogger</logger> -->
	</loggers>
	
	<!-- 正则表达式过滤器，若开启调用栈输出，会忽略以下配置的调用栈 -->
	<excludes>
		<exclude>org.apache.*</exclude>
		<exclude>java.*</exclude>
		<exclude>javax.*</exclude>
		<exclude>com.sun.*</exclude>
		<exclude>oracle.*</exclude>
		<exclude>com.alibaba.druid*</exclude>
		<exclude>*$$EnhancerByCGLIB$$*</exclude>
	</excludes>
</monitor>
```
#### 驱动类替换
将程序使用的驱动类名driverClassName(如com.mysql.jdbc.Driver或oracle.jdbc.OracleDriver）替换为john.walker.spi.CommonProxyDriver
#### 日志输出示例
```
SQL代理： select * from dd where id = ? and hire_date = ? limit 10
SQL参数： [1=1, 2=2016-11-23]
SQL耗时： 1毫秒
调用栈如下：
Test$1.test:27
Test$1.run:16
```
#### JAR下载地址
https://gitee.com/johnnie_walker/dbtrace/attach_files