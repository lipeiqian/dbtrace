package john.walker.log;


/**
 * 日志打印到控制台
 *
 * @author 30san
 *
 */
public class ConsoleLogger extends AbstractLogger {

	@Override
	public void debug(String msg) {
		System.out.println(msg);
	}

	@Override
	public void info(String msg) {
		System.out.println(msg);
	}

	@Override
	public void error(String msg) {
		System.out.println(msg);
	}

	@Override
	public void fatal(String msg) {
		System.out.println(msg);
	}

	@Override
	public void warn(String msg) {
		System.out.println(msg);
	}

	@Override
	public void newLine() {
		System.out.println();
	}
}
